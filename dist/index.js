"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const knex_config_1 = require("./config/knex.config");
async function createDatabase() {
    await knex_config_1.sqliteKnex.raw("CREATE TABLE CONFIGITEM (ITEMID varchar(50) NOT NULL,ITEMNAME varchar(512) DEFAULT NULL,ITEMTYPE varchar(50) DEFAULT NULL,ITEMDESCRIPTION varchar(4000) DEFAULT ' ',ITEMIDALIAS varchar(50) DEFAULT NULL,PROJECTID varchar(50) DEFAULT '0',CREATEDBY varchar(50) DEFAULT NULL,UPDATEDBY varchar(50) DEFAULT NULL,CREATIONDATE date DEFAULT NULL,UPDATIONDATE date DEFAULT NULL,ISDELETED int(1) DEFAULT '0',DELETIONDATE date DEFAULT NULL,GRAPH_DATA longtext ,GRAPH_IMAGE longtext ,PRIMARY KEY (ITEMID));");
}
exports.default = createDatabase();
//# sourceMappingURL=index.js.map